#!/usr/bin/python3

"""
Copyright 2014 imbadatchoosingusernames on GitLab.com.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
 "  TODO: Need to see if there's a more common place to put this.
 "
 "  wordgrid.py
 "
 "  INPUT: newline separated strings WORDS, and two integers: R and C
 "  OUTPUT: grid of R rows and C columns where each is one of WORDS
 "  EXAMPLE:
 "      > echo 'are\ncat\ncop\nore\npen\nten' | wordgrid -
 "      [['C','A','T']
 "       ['O','R','E']
 "       ['P','E','N']]
"""

import argparse
import sys
import itertools


def words_of_length(worditer, wordlength):
    """Take a iterator of strings and return only those of a certain length.

    >>> words_of_length(['do','raye','mii'],3) #doctest: +ELLIPSIS
    <generator object <genexpr> at 0x...>
    >>> list(words_of_length(['do','raye','mii'],3))
    ['mii']
    """
    # TODO: operate on multiple lengths at the same time
    return (x for x in worditer if len(x) == wordlength)


def tree(words):
    """Reorganize an iterable of words into a recursive dictionary.

    Each dictionary has keys of all unique letters in that position.
    The value for each key is a tree of remaining letters.
    Every leaf is an empty dictionary. See test_wordgrid for more examples.

    >>> test_tree = {'r': {'o': {'o': {}, 't': {}}, 'u': {'m': {}, 'n': {}}}}
    >>> tree(['roo','rot','rum','run']) == test_tree
    True
    """
    # TODO: rewrite tree to accept unlimited and unordered sequences
    t = {}
    for k, g in itertools.groupby(words, lambda x: x[:1]):
        if k:
            t[k] = tree(map(lambda x: x[1:], g))
    return t


def next_position_generator(w, h):
    """Define the path to exploring the grid.

    Starts in the top left corner, and alternates between
    expanding columns, then rows: A1, A2, B1, B2, etc.

    Goes to the end of all completed rows before starting
    a new one: C1, C2, A3, B3, C3, etc.

    >>> next_position_generator(1,1) #doctest: +ELLIPSIS
    <generator object next_position_generator at 0x...>
    """
    for i in range(1, min(h, w) + 1):
        for j in range(1, i + 1):
            if i == j:
                yield (i, j)
            else:
                yield (i, j)
                yield (j, i)

    if w > h:
        for k in range(h + 1, w + 1):
            for l in range(1, h + 1):
                yield (l, k)

    elif w < h:
        for k in range(w + 1, h + 1):
            for l in range(1, w + 1):
                yield (k, l)


def next_position_counter(depth, w, h):
    """Retrieve the appropriate position in the grid based on depth.

    Small wrapper function on next_position_generator that allows the
    desired position to be called directly.

    >>> next_position_counter(0,3,3)
    (1, 1)
    >>> next_position_counter(1,3,3)
    (2, 1)
    >>> next_position_counter(6,3,3)
    (3, 2)
    >>> next_position_counter(8,3,3)
    (3, 3)

    >>> for x in range(8): # doctest: +SKIP
    ...     next_position_counter(x,3,3)
    (1, 1)
    (2, 1)
    (1, 2)
    (2, 2)
    (3, 1)
    (3, 2)
    (1, 3)
    (2, 3)
    (3, 3)
    """
    next_position_iterator = next_position_generator(w, h)
    for i in range(depth):
        next_position_iterator.__next__()
    return next_position_iterator.__next__()


def create_empty_grid(w, h):
    """Initialize an appropriate sized grid based on width and height.

    >>> create_empty_grid(3,3)
    [[[], [], []], [[], [], []], [[], [], []]]
    """
    empty_grid = []
    empty_row = []
    for y in range(w):
        empty_row.append([])
    for x in range(h):
        empty_grid.append(empty_row[:])
    return empty_grid


def assemble_strings_for_testing(grid, pos):
    """Take a grid and constructs strings out of the row and column in pos.

    >>> assemble_strings_for_testing([['A1','A2','A3','A4'],
    ...                               ['B1','B2','B3','B4'],
    ...                               ['C1','C2','C3','C4'],
    ...                               ['D1','D2','D3','??']],
    ...                               (4,4))
    ('A4B4C4', 'D1D2D3')
    """
    # TODO: split this into two functions
    horizontal_string, vertical_string = '', ''

    # build horizontal string
    for row in range(pos[1] - 1):
        vertical_string += grid[pos[0] - 1][row]

    # build vertical string
    for column in range(pos[0] - 1):
        horizontal_string += grid[column][pos[1] - 1]

    return horizontal_string, vertical_string


def focus_tree(letters, _tree):
    """Crawl the tree and return the relevant letters.

    >>> test = focus_tree('a',{'a':{'m':{},'n':{}},'o':{'f':{},'n':{}}})
    >>> sorted(list(test)) == ['m', 'n']
    True
    """
    if letters == '':
        result = _tree.keys()
    elif letters[0] in _tree:
        result = focus_tree(letters[1:], _tree[letters[0]])
    else:
        result = None
    return result


def print_grid(grid):
    """Print a pretty version of the grid.

    >>> print_grid([['a','t'],['n','o']])
    a t
    n o
    """
    for row in grid:
        print(' '.join(row))


def main(args):
    """Do all the one-time work, then runs find_grids recursively."""
    # open dictionary files, convert to trees, and close them
    with open(args.wordlist) as f:
        horizontal_tree = tree(words_of_length(f.read().splitlines(), args.c))
    with open(args.wordlist) as f:
        vertical_tree = tree(words_of_length(f.read().splitlines(), args.r))

    def focus_both_trees(s):
        """Run focus_trees on both trees simultaneously.

        Takes a tuple of two strings, and for each, crawls a tree, returning
        the remaining subtree at the end of that path. Returns results in a
        list.
        """
        return list(focus_tree((s[0]), horizontal_tree)), list(
            focus_tree(s[1], vertical_tree))

    def find_grids(grid, w, h, depth):
        """Recursively attempt to find valid wordgrids."""
        pos = next_position_counter(depth, args.c, args.r)
        # get test strings and focus both trees
        horizontal_letters, vertical_letters = focus_both_trees(
            assemble_strings_for_testing(grid, pos))
        # check for matches in the horizontal and vertical tree
        for letter in (x for x in horizontal_letters if x in vertical_letters):
            grid[pos[0] - 1][pos[1] - 1] = letter

            # if there are still grid positions to check
            try:
                # check them recursively
                find_grids(grid, w, h, depth + 1)
            # if there aren't grid positions left
            except StopIteration:
                # TODO: combine with new assemble_strings_for_testing function
                if not args.symmetrical:
                    hs, vs = '', ''
                    for column in grid[0]:
                        hs += column
                    for row in grid:
                        vs += row[0]

                    if hs != vs:
                        # we have a full grid, print it
                        print_grid(grid)
                        input('')

                else:
                    # we have a full grid, print it
                    print_grid(grid)
                    input('')

            # reset the grid because I don't know how to limit the scope
            grid[pos[0] - 1][pos[1] - 1] = []

    find_grids(create_empty_grid(args.c, args.r), args.c, args.r, 0)

if __name__ == "__main__":
    import doctest
    doctest.testmod()

    parser = argparse.ArgumentParser(
        prog='wordgrid',
        description='Generate grids where every row and column is a word.'
    )
    parser.add_argument(
        'wordlist',
        metavar="word list",
        # type=argparse.FileType('r'),
        help='defines strings that are considered words; "-" is stdin \
        (look in /usr/share/dict/)'
    )
    # TODO: Write to file/sys.stdout
    # TODO: ArgumentTypeErrors to prevent negative values?
    parser.add_argument(
        '-c',
        nargs='?',
        default=3,
        type=int,
        help='number of columns, width of grid, \
        and length of horizontal words (default: %(default)s)'
    )
    parser.add_argument(
        '-r',
        nargs='?',
        default=3,
        type=int,
        help='number of rows, height of grid, \
        and length of vertical words (default: %(default)s)')
    parser.add_argument(
        '--symmetrical',
        action='store_true',
        default=False,
        help='include grids that are symmetrical \
        (first column is the same as the first row, \
        and so on for the rest of the grid)'
    )
    parser.add_argument('--version', action='version', version='%(prog)s 0.1')
    sys.exit(main(parser.parse_args()))
