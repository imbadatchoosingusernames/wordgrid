#! /bin/python

import wordgrid
import unittest


class TestWordsOfLengthFunction(unittest.TestCase):

    def test_empty(self):
        self.assertEqual(list(wordgrid.words_of_length([], 1)), [])

    def test_zero(self):
        l = ['whoa', 'buddy']
        self.assertEqual(list(wordgrid.words_of_length(l, 0)), [])

    def test_same(self):
        l = ['right', 'stuff']
        self.assertEqual(list(wordgrid.words_of_length(l, 5)), l)

    def test_match_one(self):
        l = ['hey', 'there']
        self.assertEqual(list(wordgrid.words_of_length(l, 3)), ['hey'])


class TestTreeFunction(unittest.TestCase):

    def test_empty(self):
        self.assertEqual(wordgrid.tree({}), {})

    def test_simple(self):
        self.assertEqual(
            wordgrid.tree(['cat']),
            {'c': {'a': {'t': {}}}}
        )

    def test_multiple(self):
        self.assertEqual(
            wordgrid.tree(['bus', 'fox']),
            {'b': {'u': {'s': {}}}, 'f': {'o': {'x': {}}}}
        )

    def test_branch(self):
        self.assertEqual(
            wordgrid.tree(['din', 'dad']),
            {'d': {'a': {'d': {}}, 'i': {'n': {}}}}
        )

    def test_complex(self):
        wordlist = sorted([
            'pizza',
            'plane',
            'plant',
            'ports',
            'pleat',
            'pores',
            'pours',
            'where',
            'whips',
            'woosh',
            'wares',
            'wants',
            'warns'])
        output = {
            'p': {'i': {'z': {'z': {'a': {}}}},
                  'l': {'a': {'n': {'e': {}, 't': {}}},
                        'e': {'a': {'t': {}}}},
                  'o': {'r': {'e': {'s': {}}, 't': {'s': {}}},
                        'u': {'r': {'s': {}}}}},
            'w': {'a': {'n': {'t': {'s': {}}},
                        'r': {'e': {'s': {}},
                              'n': {'s': {}}}},
                  'h': {'e': {'r': {'e': {}}},
                        'i': {'p': {'s': {}}}},
                  'o': {'o': {'s': {'h': {}}}}}}
        self.assertEqual(wordgrid.tree(wordlist), output)


class TestFocusTreeFunction(unittest.TestCase):

    def setUp(self):
        self.test_tree = wordgrid.tree(
            ['are', 'can', 'cap', 'cop', 'ore', 'pen', 'ten'])

    def test_fail(self):
        self.assertIsNone(wordgrid.focus_tree('ma', self.test_tree))

    def test_match(self):
        self.assertEqual(wordgrid.focus_tree('cap', self.test_tree), {}.keys())

    def test_incomplete_match(self):
        self.assertEqual(wordgrid.focus_tree('ca', self.test_tree),
                         {'n': {}, 'p': {}}.keys())

    def test_too_long(self):
        self.assertIsNone(wordgrid.focus_tree('tent', self.test_tree))

    def test_empty(self):
        self.assertEqual(
            sorted(
                list(
                    wordgrid.focus_tree(
                        '', self.test_tree))), [
                'a', 'c', 'o', 'p', 't'])

if __name__ == '__main__':
    unittest.main()
